function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); 
  var txtValor = document.getElementById("txtValor"); 
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  
}
function leerDeSessionStorage() { 
  var txtClave = document.getElementById("txtClave"); 
  var clave = txtClave.value;   
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
}
function eliminaEnSessionStorage() {  
  var txtClave = document.getElementById("txtClave");  
  var clave = txtClave.value;
  sessionStorage.removeItem(clave); 
  spanValor.innerText = "clave " + clave + " eliminada";
}

function limpiarEnSessionStorage() {  
  
  sessionStorage.clear();
  spanValor.innerText = "Limpieza de datos en SessionStorage";
}

function elementosEnSessionStorage() {  
  var numero = sessionStorage.length;
  spanValor.innerText = numero + " Elemento(s) grabado(s). ";
  
}